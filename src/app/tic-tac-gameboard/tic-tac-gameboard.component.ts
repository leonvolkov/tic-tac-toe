import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { TicTacCell } from '../tic-tac-cell';
import { Player } from '../player';
import { TicTacGameplayService } from '../tic-tac-gameplay.service';

@Component({
  selector: 'app-tic-tac-gameboard',
  templateUrl: './tic-tac-gameboard.component.html',
  styleUrls: ['./tic-tac-gameboard.component.scss']
})
export class TicTacGameboardComponent implements OnInit {
  public boardBase = 3;
  public boardCells: TicTacCell[];
  public players: Player[];
  public curPlayer: Player;
  public gameInProgress = true;
  private eptyCellsNum: number;

  constructor(public gps: TicTacGameplayService, private messageService: MessageService) {
    this.initCells();
    this.initPlayers();
    this.curPlayer = this.players[0];
  }

  ngOnInit() {

  }

  private togglePlayers() {
    if (this.players.indexOf(this.curPlayer) === 0) {
      this.curPlayer = this.players[1];
    } else {
      this.curPlayer = this.players[0];
    }
  }

  private initCells() {
    this.boardCells = [];
    this.eptyCellsNum = Math.pow(this.boardBase, 2);
    for (let i = 0; i < this.eptyCellsNum; i++) {
      this.boardCells.push(new TicTacCell());
    }
  }

  private initPlayers() {
      this.players = [];
      this.players.push(new Player('Player X', 'x'));
      this.players.push(new Player('Player O', 'o'));
      this.players.push(new Player('Draw'));
  }

  public onCellClick(event, cell: TicTacCell) {
    if (!cell.empty || !this.gameInProgress) {
      return;
    }
    cell.fillCell(this.curPlayer);
    this.gameInProgress = this.gps.getGameStatus(this.boardCells);
    if (this.gameInProgress) {
      if (--this.eptyCellsNum === 0) {
        this.gameInProgress = false;
        this.players[2].updateScore();
        this.messageService.add({severity: 'info', summary: 'Draw', detail: 'Try to play again.'});
      } else {
        this.togglePlayers();
      }
    } else {
      this.messageService.add({severity: 'success', summary: this.curPlayer.name + ' Wins!', detail: 'Congratulations!'});
    }
  }

  public newGame(event) {
    event.preventDefault();
    this.initCells();
    this.curPlayer = this.players[0];
    this.gameInProgress = true;
  }

  public clearGames(event) {
    event.preventDefault();
    this.newGame(event);
    this.initPlayers();
  }

}
