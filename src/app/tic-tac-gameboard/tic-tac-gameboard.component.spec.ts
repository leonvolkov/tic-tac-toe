import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicTacGameboardComponent } from './tic-tac-gameboard.component';

describe('TicTacGameboardComponent', () => {
  let component: TicTacGameboardComponent;
  let fixture: ComponentFixture<TicTacGameboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicTacGameboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicTacGameboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
