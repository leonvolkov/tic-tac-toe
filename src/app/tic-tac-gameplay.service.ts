import { Injectable } from '@angular/core';
import { Player } from './player';
import { TicTacCell } from './tic-tac-cell';

@Injectable({
  providedIn: 'root'
})
export class TicTacGameplayService {

  constructor() { }

  public getGameStatus (cells: TicTacCell[]): boolean {
    const winningLines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];

    for (const line of winningLines) {
      if (cells[line[0]].sign !== null && cells[line[0]].sign === cells[line[1]].sign && cells[line[1]].sign === cells[line[2]].sign) {
        cells[line[0]].emphasis = cells[line[1]].emphasis = cells[line[2]].emphasis = true;
        cells[line[0]].player.updateScore();
        return false;
      }
    }
    return true;
  }
}
