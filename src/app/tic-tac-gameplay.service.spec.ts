import { TestBed } from '@angular/core/testing';

import { TicTacGameplayService } from './tic-tac-gameplay.service';

describe('TicTacGameplayService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TicTacGameplayService = TestBed.get(TicTacGameplayService);
    expect(service).toBeTruthy();
  });
});
