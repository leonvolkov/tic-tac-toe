import { Player } from './player';

export class TicTacCell {
  public empty = true;
  public sign: string = null;
  public emphasis = false;
  public cssClass = '';
  public player: Player = null;

  public fillCell(player: Player) {
    const sccClassDict = {
      x: 'pi-times',
      o: 'pi-circle-off'
    };
    this.cssClass = sccClassDict[player.sign];
    this.sign = player.sign;
    this.empty = false;
    this.player = player;
  }

  constructor() { }
}

