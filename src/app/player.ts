export class Player {
  public isBot: boolean;
  public score = 0;
  public name: string;
  public sign: string; // x\o\''

  constructor (name, sign = '', isBot = false) {
    this.name = name;
    this.sign = sign;
    this.isBot = isBot;
  }

  public updateScore() {
    this.score++;
    return this.score;
  }
}
